output "release_metadata" {
  description = "Map of attributes of the Helm release metadata"
  value       = module.helm-template.release_metadata
}

output "irsa_arn" {
  description = "IAM role ARN for the service account"
  value       = module.helm-template.irsa_arn
}

output "irsa_name" {
  description = "IAM role name for the service account"
  value       = module.helm-template.irsa_name
}

output "service_account" {
  description = "Name of Kubernetes service account"
  value       = module.helm-template.service_account
}
